<%-- 
    Document   : menu
    Created on : 08-04-2021, 19:38:06
    Author     : Alex
--%>

<%@page import="model.Pronostico"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="model.Menu"%>
<%@page import="model.Menu"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Menu> listaMenu = (List<Menu>)request.getAttribute("menu");
    Iterator<Menu> itMenu = listaMenu.iterator();

    List<Pronostico> listaPronostico = (List<Pronostico>)request.getAttribute("pronostico");
    Iterator<Pronostico> itPronostico = listaPronostico.iterator();

%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href='css/menu.css' rel='stylesheet' type='text/css'>
        <script src="script/menu.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
        <link rel="stylesheet" href="css/menu.css">
        <script src="script/mnu.js"></script>
    </head>
    <body>
        <nav>
          <input type="checkbox" id="check">
          <label for="check" class="checkbtn">
            <i class="fas fa-bars"></i>
          </label>
          <label class="logo">DesignX</label>
          <ul>
            <li><a class="active" href="#">Home</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Services</a></li>
            <li><a href="#">Contact</a></li>
            <li><a href="#">Feedback</a></li>
          </ul>
        </nav>
        <section></section>
        
        
        <br>
        <br>
        <br>
        <br>
        <br>

        <%
            while (itMenu.hasNext())
            {
                Menu menu = itMenu.next();%>
                
                <a href="<%= menu.getUrl() %>" target="_blank"><%= menu.getLabel() %></a>
                
            <%}
        %>
        
        
        <br> 
        
        <table>
            <tr>
                <td>Día</td>
                <td>Condición</td>
                <td>Temperatura</td>
                <td>Viento</td>
            </tr>

        <%
            while (itPronostico.hasNext())
            {
                Pronostico pron  = itPronostico.next();%>
                
            <tr>
                <td><%= pron.getDia()%></td>
                <td><%= pron.getCondicion()%></td>
                <td><%= pron.getTemperatura()%></td>
                <td><%= pron.getViento()%></td>
            </tr>

                
            <%}
        %>            
        
        </table>
        
    </body>
</html>
