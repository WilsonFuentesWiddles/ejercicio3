<%-- 
    Document   : index
    Created on : 08-04-2021, 19:11:22
    Author     : Alex
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/style.css"/>
        <script src="js/script.jj"></script>
        
    </head>
    <body>
        <div class="login-page">
          <div class="form">
              <form name="frmLogin" method="POST" action="LoginController" class="login-form">
              <input type="text" name="txtLogin" id="txtLogin" placeholder="username"/>
              <input type="password" name="txtPassword" id="txtPassword" placeholder="password"/>
              <button>login</button>
              <p class="message">Not registered? <a href="#">Create an account</a></p>
            </form>
          </div>
        </div>
    </body>
</html>
