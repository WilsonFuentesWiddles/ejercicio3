package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Autenticador;
import model.Menu;
import model.Pronostico;


@WebServlet(name = "LoginController", urlPatterns = {"/LoginController"})
public class LoginController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        String txtLogin = request.getParameter("txtLogin");
        String txtPassword = request.getParameter("txtPassword");
        
        
        //System.out.println("Login: " + txtLogin);
        //System.out.println("Password: " + txtPassword);
        
        Autenticador auth = new Autenticador(txtLogin, txtPassword);
        if (auth.validarAcceso(txtLogin, txtPassword) == true)
        {
            List<Menu> listaMenu = new ArrayList<Menu>();
            Menu m1 = new Menu("Google", "www.google.cl");
            Menu m2 = new Menu("Facebook", "www.facebook.com");
            Menu m3 = new Menu("SII", "www.sii.com");
            listaMenu.add(m1);
            listaMenu.add(m2);
            listaMenu.add(m3);
            request.setAttribute("menu", listaMenu);
            
            List<Pronostico> listaPronostico = new ArrayList<Pronostico>();
            Pronostico p1 = new Pronostico("Hoy", "Soleado", "25º", "13 km");
            Pronostico p2 = new Pronostico("Mañana", "Nublado", "25º", "13 km");
            Pronostico p3 = new Pronostico("Sabado", "Nublado", "25º", "13 km");
            listaPronostico.add(p1);
            listaPronostico.add(p2);
            listaPronostico.add(p3);
            request.setAttribute("pronostico", listaPronostico);
           
            request.getRequestDispatcher("menu.jsp").forward(request, response);            
        }
        else
            request.getRequestDispatcher("error.jsp").forward(request, response);
        
        
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
