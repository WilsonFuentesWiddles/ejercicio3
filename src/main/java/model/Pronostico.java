package model;


public class Pronostico
{
    private String dia;
    private String condicion;
    private String temperatura;
    private String viento;

    public Pronostico(String dia, String condicion, String temperatura, String viento)
    {
        this.dia = dia;
        this.condicion = condicion;
        this.temperatura = temperatura;
        this.viento = viento;
    }

    public String getDia() {
        return dia;
    }

    public String getCondicion() {
        return condicion;
    }

    public String getTemperatura() {
        return temperatura;
    }

    public String getViento() {
        return viento;
    }
    
    
    
    
    
}
